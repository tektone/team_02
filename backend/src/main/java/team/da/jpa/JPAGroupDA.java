package team.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import team.api.group.Group;
import team.api.group.GroupForm;
import team.da.GroupDALayer;
import team.da.jpa.converter.GroupMapper;
import team.da.jpa.entity.GroupEntity;
import team.da.jpa.entity.GroupLinkEntity;
import team.da.jpa.repository.GroupEntityRepository;
import team.da.jpa.repository.GroupLinkEntityRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Profile("SPRING_DATA")
public class JPAGroupDA implements GroupDALayer {

    /**
     * Зависимость на group репозиторий.
     */
    @Autowired
    private GroupEntityRepository groupRepo;

    /**
     * Зависимость на link репозиторий.
     */
    @Autowired
    private GroupLinkEntityRepository linkRepo;

    /**
     * Зависимость на маппер.
     */
    @Autowired
    private GroupMapper groupMapper;

    /**
     * Связывание пользователя и группы.
     *
     * @param userId
     * @param groupId
     * @return message
     */
    @Override
    public String linkUser(final String userId, final String groupId) {
        GroupLinkEntity entity = new GroupLinkEntity();
        entity.setPk(GroupLinkEntity.pk(userId, groupId));
        entity.setGroup(getGroupEntityById(groupId));
        linkRepo.save(entity);
        return "link added";
    }

    /**
     * Получение GroupEntity по Id.
     *
     * @param groupId
     * @return Group
     */
    public GroupEntity getGroupEntityById(final String groupId) {
        Optional<GroupEntity> groupEntity = groupRepo.findById(groupId);
        if (!groupEntity.isPresent()) {
            throw new RuntimeException("Group not found by id");
        }
        return groupEntity.get();
    }

    /**
     * Получение всех доступных групп.
     *
     * @return List
     */
    @Override
    public List<Group> getAllGroups() {
        return groupMapper.mapList(groupRepo.findAll());
    }

    /**
     * Получение группы по Id.
     *
     * @param groupId
     * @return Group
     */
    @Override
    public Group getGroupById(final String groupId) {
        return groupMapper.map(getGroupEntityById(groupId));
    }

    /**
     * Создание группы.
     *
     * @param groupForm
     * @return Group
     */
    @Override
    public Group createGroup(final GroupForm groupForm) {
        Group group = Group.builder()
                .groupId(UUID.randomUUID().toString())
                .groupName(groupForm.getGroupName())
                .description(groupForm.getDescription())
                .build();

        GroupEntity groupEntity = groupMapper.map(group);
        groupRepo.save(groupEntity);
        return group;
    }

    /**
     * Удаление группы по Id.
     *
     * @param groupId
     * @return Group
     */
    @Override
    public String deleteGroupById(final String groupId) {
        linkRepo.deleteAll(linkRepo.findAllByPkGroupId(groupId));
        groupRepo.delete(getGroupEntityById(groupId));
        return "DELETED";
    }

    /**
     * Обновление информации о группе.
     *
     * @param group
     * @return Group
     */
    @Override
    public Group updateGroupById(final Group group) {
        groupRepo.save(groupMapper.map(group));
        return group;
    }

    /**
     * получить список групп в которых состоит пользователь.
     *
     * @param userId
     * @return GroupList
     */
    @Override
    public List<Group> getGroupsByPkUserId(final String userId) {
        List<GroupLinkEntity> groupEntities = linkRepo.findAllByPkUserId(userId);
        if (groupEntities == null) {
            List<Group> groupList = new ArrayList<>();
            return groupList;
        }
        return groupMapper.mapList(groupEntities.stream()
                .map(GroupLinkEntity::getGroup)
                .collect(Collectors.toList()));
    }

    /**
     * Поместить пользователя в группу.
     *
     * @param userId
     * @param groupId
     */
    @Override
    public void putUserInGroup(final String userId, final String groupId) {
        linkUser(userId, groupId);
    }
}
