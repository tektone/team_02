package team.da.jpa.converter;

import org.mapstruct.Mapper;
import team.api.group.Group;
import team.da.jpa.entity.GroupEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GroupMapper {

    /**
     * Маппер GroupEntity -> Group.
     *
     * @param entity
     * @return group
     */
    Group map(GroupEntity entity);

    /**
     * Маппер Group -> GroupEntity.
     *
     * @param group
     * @return GroupEntity
     */
    GroupEntity map(Group group);

    /**
     * Маппер List<GroupEntity> -> List<Group>.
     *
     * @param entities
     * @return maplist
     */
    List<Group> mapList(Iterable<GroupEntity> entities);
}
