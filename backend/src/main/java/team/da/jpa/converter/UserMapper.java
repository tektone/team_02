package team.da.jpa.converter;

import org.mapstruct.Mapper;
import team.api.user.User;
import team.da.jpa.entity.UserEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    /**
     * Маппер UserEntity -> User.
     *
     * @param entity
     * @return user
     */
    User map(UserEntity entity);

    /**
     * Маппер User -> UserEntity.
     *
     * @param user
     * @return UserEntity
     */
    UserEntity map(User user);


    /**
     * Маппер List<UserEntity> -> List<User>.
     *
     * @param entities
     * @return userlist
     */
    List<User> mapList(Iterable<UserEntity> entities);
}
