package team.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Setter
@Getter
@Table(name = "\"groups\"")
public class GroupEntity {

    /**
     * ID группы.
     * Первичный ключ.
     */
    @Id
    private String groupId;

    /**
     * Название группы.
     */
    @Column(name = "group_name")
    private String groupName;

    /**
     * Описание группы.
     */
    @Column(name = "description")
    private String description;
}
