package team.da.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import team.da.jpa.entity.GroupEntity;

@Repository
public interface GroupEntityRepository extends CrudRepository<GroupEntity, String> {

    }
