package team.da.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import team.da.jpa.entity.UserEntity;


@Repository
public interface UserEntityRepository extends
        CrudRepository<UserEntity, String> {
}
