package team.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import team.api.entrant.EntrantInfo;
import team.da.EntrantDALayer;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class EntrantControllerTest {

    public static final String ENTRANT_ID = "entrantId";
    public static final String USERNAME = "username";
    @Mock
    private EntrantDALayer entrantDALayer;

    @Mock
    private EntrantInfo entrantMock;

    @Mock
    private EntrantInfo entrantMock2;

    @InjectMocks
    private EntrantController entrantController;

    private String expected;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
        when(entrantDALayer.register(entrantMock)).thenReturn(entrantMock);
        when(entrantDALayer.register(entrantMock2)).thenReturn(null);
        when(entrantMock.getId()).thenReturn(ENTRANT_ID);
        when(entrantDALayer.findByUsername(USERNAME)).thenReturn(entrantMock);

    }

    @Test
    public void register() {
        expected = entrantController.register(entrantMock);
        assertEquals(expected, ENTRANT_ID);
    }

    @Test (expected = IllegalStateException.class)
    public void registerNull() {
        entrantController.register(entrantMock2);
    }


    @Test
    public void loadUserByUsername() {
        EntrantInfo entrant = entrantController.loadUserByUsername(USERNAME);
        assertEquals(entrant, entrantMock);
    }
}