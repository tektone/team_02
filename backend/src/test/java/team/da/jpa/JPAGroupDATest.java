package team.da.jpa;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import team.api.group.Group;
import team.api.group.GroupForm;
import team.api.user.User;
import team.da.jpa.converter.GroupMapper;
import team.da.jpa.entity.GroupEntity;
import team.da.jpa.repository.GroupEntityRepository;
import team.da.jpa.repository.GroupLinkEntityRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class JPAGroupDATest {

    public static final String GROUP_ID = "groupId";
    public static final String USER_ID = "userId";
    @Mock
    private GroupEntityRepository groupRepo;

    @Mock
    private GroupLinkEntityRepository linkRepo;

    @Mock
    private GroupMapper groupMapper;

    @Mock
    private GroupForm groupForm;

    @Mock
    private Group groupMock;

    @Mock
    private GroupEntity groupEntityMock;

    private List<Group> groupList = new ArrayList<>();
    private List<GroupEntity> groupEntities = new ArrayList<>();
    private final GroupEntity groupEntity = new GroupEntity();
    private final Optional<GroupEntity> groupEntityOptional = Optional.of(groupEntity);
    private final Optional<GroupEntity> groupEntityOptional2 = Optional.ofNullable(groupEntityMock);


    String expected;

    @InjectMocks
    private JPAGroupDA jpaGroupDA;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
        when(groupRepo.findAll()).thenReturn(groupEntities);
        when(groupMapper.map(groupEntity)).thenReturn(groupMock);
        when(groupRepo.findById(GROUP_ID)).thenReturn(groupEntityOptional);
    }

    @Test
    public void linkUser() {
        expected = jpaGroupDA.linkUser(USER_ID, GROUP_ID);
        assertEquals("link added", expected);
    }

    @Test
    public void getGroupEntityById() {
        GroupEntity entity = jpaGroupDA.getGroupEntityById(GROUP_ID);
        assertEquals(entity, groupEntity);

    }

    @Test
    public void getAllGroups() {
        when(groupMapper.mapList(groupEntities)).thenReturn(groupList);
        List<Group> list = jpaGroupDA.getAllGroups();

        assertEquals(list, groupList);
    }

    @Test
    public void getGroupById() {
        Group group = jpaGroupDA.getGroupById(GROUP_ID);
        assertEquals(group, groupMock);
    }

    @Test
    public void createGroup() {
        when(groupForm.getGroupName()).thenReturn("groupName");
        when(groupForm.getDescription()).thenReturn("description");

        Group group = jpaGroupDA.createGroup(groupForm);

        assertEquals("groupName", group.getGroupName());
        assertEquals("description", group.getDescription());
        assertNotNull(group.getGroupId());
    }

    @Test
    public void deleteGroupById() {
        expected = jpaGroupDA.deleteGroupById(GROUP_ID);
        assertEquals("DELETED", expected);
    }

    @Test(expected = RuntimeException.class)
    public void deleteGroupByIdNull() {
        when(groupRepo.findById(GROUP_ID)).thenReturn(groupEntityOptional2);
        expected = jpaGroupDA.deleteGroupById(GROUP_ID);
    }

    @Test
    public void updateGroupById() {
        when(groupMapper.map(groupMock)).thenReturn(groupEntity);
        Group group = jpaGroupDA.updateGroupById(groupMock);
        assertEquals(group, groupMock);
    }

    @Test
    public void getGroupsByPkUserId() {
//так же непонятно
        List<Group> list = jpaGroupDA.getGroupsByPkUserId(USER_ID);
        assertEquals(list, groupList);
    }
@Test
    public void getGroupsByPkUserIdNull() {
        when(linkRepo.findAllByPkGroupId(USER_ID)).thenReturn(null);
        List<Group> list = jpaGroupDA.getGroupsByPkUserId(USER_ID);
        List<Group> expected = new ArrayList<>();
        assertEquals(list, expected);
    }

    @Test
    public void putUserInGroup() {
//так же непонятно
        jpaGroupDA.putUserInGroup(USER_ID, GROUP_ID);
    }
}