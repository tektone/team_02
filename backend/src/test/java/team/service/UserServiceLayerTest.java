package team.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import team.api.user.User;
import team.api.user.UserForm;
import team.da.UserDALayer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class UserServiceLayerTest {

    public static final String USER_ID = "123";
    public static final String GROUP_ID = "groupId";


    @Mock
    private User userMock;

    @Mock
    private User userMock2;

    @Mock
    private UserForm userFormMock;

    @Mock
    public UserDALayer daLayer;

    @InjectMocks
    private UserServiceLayer userServiceLayer;

    private String expected;

    private List<User> userList = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        openMocks(this);
        when(daLayer.getUserById(USER_ID)).thenReturn(userMock);
        when(daLayer.createUser(userFormMock)).thenReturn(userMock);
        when(daLayer.getAllUsers()).thenReturn(userList);
        when(daLayer.getUsersByPkGroupId(GROUP_ID)).thenReturn(userList);
        when(daLayer.updateUser(userMock)).thenReturn(userMock2);
    }

    @Test
    public void getUserById() {
        User user = userServiceLayer.getUserById(USER_ID);
        assertEquals(userMock, user);
    }

    @Test
    public void deleteUserById() {
        expected = userServiceLayer.deleteUserById(USER_ID);
        assertEquals("DELETED", expected);
    }

    @Test
    public void createUser() {
        User user = userServiceLayer.createUser(userFormMock);
        assertEquals(userMock, user);
    }

    @Test
    public void getAllUsers() {
        List<User> list = userServiceLayer.getAllUsers();
        assertEquals(list, userList);
    }

    @Test
    public void updateUser() {
        User user = userServiceLayer.updateUser(userMock);
        assertEquals(user, userMock2);
    }

    @Test
    public void getUsersByGroupId() {
        List<User> list = userServiceLayer.getUsersByGroupId(GROUP_ID);
        assertEquals(list, userList);
    }

    @Test
    public void putUserInGroup() {

    }
}