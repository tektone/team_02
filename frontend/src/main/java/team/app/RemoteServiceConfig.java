package team.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import team.api.entrant.EntrantService;
import team.api.group.GroupService;
import team.api.user.UserService;
import team.backend.RestServiceInvocationHandler;

import java.lang.reflect.Proxy;

@Configuration
@Profile("REST")
@SuppressWarnings("unchecked")
public class RemoteServiceConfig {

    /**
     * Создаем rest-прокси для UserService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public UserService userServiceRest(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/user");
        return getProxy(handler, UserService.class);
    }

    /**
     * Создаем rest-прокси для GroupService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public GroupService groupService(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/group");
        return getProxy(handler, GroupService.class);
    }

    /**
     * Создаем rest-прокси для UserService.
     *
     * @param handler
     * @return entrantService
     */
    @Bean
    public EntrantService entrantService(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/entrant");
        return getProxy(handler, EntrantService.class);
    }

    private <T> T getProxy(final RestServiceInvocationHandler handler, final Class<T>... tClass) {
        return (T) Proxy.newProxyInstance(this.getClass().getClassLoader(), tClass, handler);
    }
}
