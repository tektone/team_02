package team.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import team.api.group.GroupService;
import team.api.user.UserService;
import team.stub.group.StubGroup;
import team.stub.user.StubUser;

@Configuration
@Profile("STUBS")
public class StubsConfig {

    /**
     * Заглушка сервиса.
     *
     * @return bean
     */
    @Bean
    public GroupService groupServiceBean() {
        return new StubGroup();
    }

    /**
     * Заглушка сервиса.
     * @param groupService
     * @return bean
     */
    @Bean
    public UserService userServiceBean(final GroupService groupService) {
        return new StubUser(groupService);
    }

}
