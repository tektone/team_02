package team.api.entrant;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
@Builder
public class EntrantInfo {
    /**
     * UserId.
     */
    private String id;

    /**
     * Username.
     */
    private String username;


    /**
     * Password.
     */
    private String password;

    /**
     * Role.
     */
    private String roles;

    /**
     * Enabled.
     */
    private Boolean enabled;
}
